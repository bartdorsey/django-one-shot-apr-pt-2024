from django.contrib import admin
from todos.models import TodoItem, TodoList

# Register your models here.


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ["task", "due_date", "is_completed"]


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ["id", "name"]
