from django.urls import path
from todos.views import todo_list_view, todo_list_detail_view

urlpatterns = [
    path("", todo_list_view, name="todo_list_list"),
    path("<int:id>/", todo_list_detail_view, name="todo_list_detail"),
]
