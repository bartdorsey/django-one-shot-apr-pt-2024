from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from todos.models import TodoList


# Create your views here.
def todo_list_view(request: HttpRequest) -> HttpResponse:
    todolists = TodoList.objects.all()
    context = {"todolists": todolists}
    return render(request, "todo_list.html", context)


def todo_list_detail_view(request: HttpRequest, id: int) -> HttpResponse:
    todo_list = TodoList.objects.get(id=id)
    todo_items = todo_list.items.all()
    print(todo_items)
    return render(
        request,
        "todo_details.html",
        {"todo_list": todo_list, "todo_items": todo_items},
    )
